import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:myapp/screen/model.dart';
// ignore: unused_import
import 'package:myapp/screen/result.dart';

class FormSection extends StatefulWidget {
  @override
  _FormSectionState createState() => _FormSectionState();
}

class _FormSectionState extends State<FormSection> {
  final _formKey = GlobalKey<FormState>();
  Model model = Model();
  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return MaterialApp(
      title: 'New Screen',
      home: Scaffold(
          appBar: AppBar(
            title: Text('Welcome to Form Section'),
          ),
          body: Padding(
            padding: const EdgeInsets.all(32.0),
            child: Center(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    TextFormField(
                      decoration: const InputDecoration(
                          icon: const Icon(Icons.person),
                          hintText: 'Enter your name',
                          labelText: 'Name',
                          border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.blue, width: 5.0))),
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]")),
                      ],
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        model.name = value;
                      },
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                          icon: const Icon(Icons.phone),
                          hintText: 'Enter a phone number',
                          labelText: 'Phone',
                          border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.blue, width: 5.0))),
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp("[0-9]")),
                      ],
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter  phone number';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        model.phone = value;
                      },
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                          icon: const Icon(Icons.calendar_today),
                          hintText: 'Enter your date of birth',
                          labelText: 'Dob',
                          border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.blue, width: 5.0))),
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp("[0-9-]")),
                      ],
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter  date';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        model.dob = value;
                      },
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                          icon: const Icon(Icons.email),
                          hintText: 'Enter your EmailId',
                          labelText: 'Email',
                          border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.blue, width: 5.0))),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter Email id';
                        }

                        if (value.indexOf('@') < 1 ||
                            (value.lastIndexOf('.') - value.indexOf('@') < 2)) {
                          return 'please enter valid email-id';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        model.email = value;
                      },
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                          icon: const Icon(Icons.lock),
                          hintText: 'Enter your Password',
                          labelText: 'Password',
                          border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.blue, width: 5.0))),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter valid password';
                        }
                        if (value.length < 6) {
                          return 'Password too Short';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        model.password = value;
                      },
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 50.0, top: 40.0),
                        width: 300.0,
                        height: 80.0,
                        child: RaisedButton(
                          child: Text(
                            'Submit',
                            style:
                                TextStyle(fontSize: 20.0, color: Colors.white),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          color: Colors.green,
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ResultSection(model: this.model)),
                              );
                              print('Form was submitted');
                            }
                          },
                        )),
                    Container(
                        padding: const EdgeInsets.only(left: 50.0, top: 40.0),
                        width: 300.0,
                        height: 80.0,
                        child: RaisedButton(
                            child: const Text(
                              'Back',
                              style: TextStyle(
                                  fontSize: 20.0, color: Colors.black),
                            ),
                            shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                            color: Colors.yellow,
                            onPressed: () {
                              Navigator.pop(
                                context,
                              );
                            })),
                  ],
                ),
              ),
            ),
          )),
    );
  }
}
