import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:myapp/screen/model.dart';

class ResultSection extends StatelessWidget {
  final Model model;

  ResultSection({this.model});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'New Screen',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Successful'),
        ),
        body: Container(
            margin: const EdgeInsets.all(10.0),
            child: Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                     padding: EdgeInsets.all(16.0),

                      child: Text(
                        'Submitted Details',
                        softWrap: true,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w900,
                          fontFamily: "Georgia",
                        ),
                      ),
                    ),
                    Text(model.name, style: TextStyle(fontSize: 22)),
                    Text(model.phone, style: TextStyle(fontSize: 22)),
                    Text(model.dob, style: TextStyle(fontSize: 22)),
                    Text(model.email, style: TextStyle(fontSize: 22)),
                    Text(model.password, style: TextStyle(fontSize: 22)),
                  ]),
            ),
            ),
      ),
    );
  }
}
