import 'package:flutter/material.dart';
import 'package:myapp/screen/buttonsection.dart';
import 'screen/textSection.dart';
import 'screen/titleSection.dart';
import 'package:myapp/screen/formsection.dart';

void main() {
  runApp(MaterialApp(title: 'Navigation', home: MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Flutter'),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(top: 16),
                child: Image.asset('assets/images/lake.jpeg'),
              ),
              TitleSection(),
              TextSection(),
              ButtonSection(),
              Container(
                  padding: const EdgeInsets.only(top: 40.0,bottom: 20.0),
                  width: 300.0,
                  height: 100.0,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => FormSection()),
                      );
                    },
                    shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0)),
                    child: Text('Registration Form ', style:
                                TextStyle(fontSize: 20.0, color: Colors.white),),
                    color: Colors.pink,
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
